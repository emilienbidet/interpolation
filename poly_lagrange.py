import numpy as np
import numpy.polynomial.polynomial as nppol
import matplotlib.pyplot as plt

def Q(racines,i) :
	racinesSansI = []
	for ra in range(0,len(racines)):
		if ra!=i:
			racinesSansI.append(racines[ra])
	RiX = nppol.polyfromroots(racinesSansI)
	bi = nppol.polyval(racines[i],RiX)
	return (1/bi)*RiX

def base_lagrange(px) :
	tabPoly = []
	for ra in range(0,len(px)):
		tabPoly.append(Q(px,ra))
	return tabPoly

def poly_lagrange(px,py) :
	tabPoly = base_lagrange(px)
	polyFinal = nppol.polyzero
	for ra in range(0,len(px)):
		polyFinal = nppol.polyadd(polyFinal,tabPoly[ra]*py[ra])

	return polyFinal

def f(x) :
    return 5*x*np.sin(x)/(1+x*x)

# Code 4 : Test sur l'exemple de départ
# px = [0,2,3,5]
# py = [1,-5,-8,16]
# print(poly_lagrange(px,py))
# Remarque : On obtient bien les mêmes résultats que dans la précédente.


# Code 5 : La fonction poly_lagrange

# Affichage de la fonction f.
x = np.linspace(0,10,100)
y = f(x)
c_f , = plt.plot(x, y, color='blue', linestyle='-', linewidth=2)
c_f.set_label('f')

# Utilisation de la méthode poly_lagrange pour trouver la fonction f.
px = np.linspace(0,10,6)
py = []
for val in px:
	py.append(f(val))
f = poly_lagrange(px,py)

# Calcul des ordonnées avec la fonction f trouvée.
y = []
for val in x:
	y.append(nppol.polyval(val,f))
c_ap , = plt.plot(x, y, color='green', linestyle='-', linewidth=2)
c_ap.set_label('approx.')

# Affichage des points de la légende et du graphique.
p = plt.scatter(px,py,color = 'orange', marker='o')
p.set_label('points')

plt.grid(color = 'grey', linestyle = 'dashed')
plt.legend()
plt.show()


