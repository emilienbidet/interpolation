import numpy as np
import numpy.polynomial.polynomial as nppol
import matplotlib.pyplot as plt

def eval_cubic_spline(x,cs) :
    for borne,poly in cs :
        if x <= borne :
            return nppol.polyval(x,poly)
    return 0

def get_system_to_solve(px,py) :
    n = len(px)
    nbL = 4*n-4
    A = np.zeros((nbL,nbL))
    B = np.zeros((nbL,1))

    # Remplissage de A
    # Interpolation 
    cptX = 0;
    cptC = -4;
    for ligne in range(0,2*n-2):
        if ligne % 2 == 0:
            cptC += 4
        else:
            cptX += 1
        pos = 0
        for pui in range(3,-1,-1):
            A[ligne][cptC+pos] = px[cptX]**pui
            pos += 1

    # Dérivabilité
    cptX = 1
    cptC = 0
    for ligne in range(2*n-2,2*n-2+n-2):
        pos = 0
        coef = 3
        for pui in range(2,-1,-1):
            A[ligne][cptC+pos] = px[cptX]**pui * coef
            pos += 1
            coef -= 1
        pos = 4
        coef = -3
        for pui in range(2,-1,-1):
            A[ligne][cptC+pos] = px[cptX]**pui * coef
            pos += 1
            coef += 1
        cptX += 1
        cptC += 4

    # Dérivabilité seconde 
    cptX = 1
    cptC = 0
    for ligne in range(2*n-2+n-3+1,2*n-2+(n-2)*2):
        pos = 0
        coef = 6
        for pui in range(1,-1,-1):
            A[ligne][cptC+pos] = px[cptX]**pui * coef
            pos += 1
            coef -= 4
        pos = 4
        coef = -6
        for pui in range(1,-1,-1):
            A[ligne][cptC+pos] = px[cptX]**pui * coef
            pos += 1
            coef += 4
        cptX += 1
        cptC += 4

    # Condition arbitraire 1
    cptX = 0
    cptC = 0
    pos = 0
    coef = 3
    for pui in range(2,-1,-1):
        A[nbL-2][cptC+pos] = px[cptX]**pui * coef
        pos += 1
        coef -= 1
    cptX = n-1
    cptC = (n-2)*4
    pos = 0
    coef = -3
    for pui in range(2,-1,-1):
        A[nbL-2][cptC+pos] = px[cptX]**pui * coef
        pos += 1
        coef += 1

    # Condition arbitraire 2
    cptX = 0
    cptC = 0
    pos = 0
    coef = 6
    for pui in range(1,-1,-1):
        A[nbL-1][cptC+pos] = px[cptX]**pui * coef
        pos += 1
        coef -= 4
    cptX = n-1
    cptC = (n-2)*4
    pos = 0
    coef = -6
    for pui in range(1,-1,-1):
        A[nbL-1][cptC+pos] = px[cptX]**pui * coef
        pos += 1
        coef += 4

    # Remplissage de B
    cptY = 0
    for y in range(0,2*n-2):
        if y%2!=0:
            cptY += 1
        B[y] = py[cptY]
    return A,B

def get_cubic_splines(px,py) :
    n = len(px)
    A,B = get_system_to_solve(px,py)
    X = np.linalg.inv(A)@B
    cs = []
    decX = 0
    for x in range(0,n-1):
        P = []
        xi1 = px[x+1]
        for val in range(3,-1,-1):
            P.append(X[val+decX][0])
        cs.append((xi1,P))
        decX += 4
    return cs

def get_cubic_splines_2d(px,py) :
    t = np.linspace(0,1,len(px))
    cs_x = get_cubic_splines(t,px)
    cs_y = get_cubic_splines(t,py)
    return cs_x,cs_y

def f(x) :
    return 5*x*np.sin(x)/(1+x*x)

# Code 7 : Test sur l'exemple de départ
# px = [0,2,3,5]
# py = [1,-5,-8,16]
# print(get_cubic_splines(px,py))
# Remarque : On obtient bien les mêmes résultats que dans la question 5.c

# Code 8 : La fonction f
# Affichage de la fonction f.
x = np.linspace(0,10,100)
y = f(x)
plt.subplot(1,2,1)
c_f , = plt.plot(x, y, linestyle = '-', linewidth = 2)
c_f.set_label('f')

# Cacul des images et affichage de l'approximation
px = np.linspace(0,10,6)
py = [f(x) for x in px]
cs = get_cubic_splines(px,py)
y = [eval_cubic_spline(e,cs) for e in x]
plt.subplot(1,2,1)
c_ap, = plt.plot(x,y, color = 'orange', linestyle = '-', linewidth = 2)
c_ap.set_label("approx.")

# Affichage des points de la légende et du graphique.
plt.subplot(1,2,1)
p = plt.scatter(px,py, color = 'green', marker='o')
p.set_label('points')
plt.legend()
plt.grid(color="grey",linestyle="--")


# Code 9 : La fonction get_cubic_splines_2d

# Affichage de la courbe
px = [0,2,3,0,-2,-1,0]
py = [0,-1,2,3,1,-1,0]

cs_x,cs_y = get_cubic_splines_2d(px,py)

t = np.linspace(0,1,100)
x = [eval_cubic_spline(e,cs_x) for e in t]
y = [eval_cubic_spline(e,cs_y) for e in t]

plt.subplot(1,2,2)
cp, = plt.plot(x,y)
cp.set_label('courbe')


# Affichage des points
p = plt.scatter(px,py, color = 'green', marker='o')
p.set_label('points')
plt.legend()

plt.grid(color="grey",linestyle="--")
plt.show()
